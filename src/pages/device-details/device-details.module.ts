import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonPullupModule } from 'ionic-pullup';
import { TranslateModule } from '@ngx-translate/core';
import { Crop } from '@ionic-native/crop';
import { DeviceDetailsPage } from './device-details';


@NgModule({
  declarations: [
    DeviceDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DeviceDetailsPage),
    IonPullupModule,
    TranslateModule.forChild()
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [Crop]
})
export class VehicleDetailsPageModule {}
