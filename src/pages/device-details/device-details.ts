import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events, Platform, ActionSheetController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { IonPullUpFooterState } from 'ionic-pullup';
import { AppVersion } from '@ionic-native/app-version';
import { FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-device-details',
  templateUrl: 'device-details.html',
})
export class DeviceDetailsPage {
  device: any = {};

  constructor(
    public navParams: NavParams,
  ) {
  }

  ionViewDidEnter() {
    console.log(this.navParams.get('device'));
    this.device = this.navParams.get('device');
    console.log('ionViewDidEnter Vehicle details page');
  }


}