import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-ac-report',
  templateUrl: 'ac-report.html',
})
export class AcReportPage implements OnInit {
  datetimeStart: any;
  datetimeEnd: any;
  islogin: any;
  portstemp: any[] = [];
 dev_id : any;
 selectedVehicle:any;
  reportData: any[] = [];
  reportArray: any[] = [];
  isreportArray:any= true;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");
  totalWorkingHours: any[];
  temparray:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCAll: ApiServiceProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    let sDate = moment();
    this.datetimeStart = moment(sDate.subtract(10, "days")).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    this.selectedVehicle = this.navParams.get('item');

    let deviceIds = [];
    let temp = this.selectedVehicle.map((item)=>{
      deviceIds.push(item.Device_ID)
    })
    console.log('deviceIds------- ', deviceIds);
    this.dev_id = deviceIds.join(",");
    console.log(' this.dev_id------- ',  this.dev_id);
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter AcReportPage');
  }

  ngOnInit() {
    this.getdevices();
  }

  getAcReporID(key) {
    console.log("key: ", key.Device_ID)
    this.dev_id = key.Device_ID;
  }

  getdevices() {
    var baseURLp = this.apiCAll.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCAll.startLoading().present();
    this.apiCAll.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCAll.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCAll.stopLoading();
          console.log(err);
        });
  }

  calculateHours(minutes) {
    let min = minutes / 60;
    let splitString = JSON.stringify(min).split('.');
    let hour = Number(splitString[0]);
    let temp = '0.' + splitString[1];
    let min1 = Number(temp) * 60;
    let a = JSON.stringify(min1).split('.');
    let min2 = Number(a[0]);

    return hour + 'hr ' + min2 + 'min';
  }

  getAcReportData() {
    console.log("hii");
    let that = this;
    this.reportData = [];
    if (that.dev_id == undefined) {
      that.dev_id = "";
    }
    this.apiCAll.startLoading().present();
    // this.apiCAll.getACReportAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
    //   .subscribe(data => {
    //     this.apiCAll.stopLoading();
    //     console.log("ac report data: ", data)
    //     this.calculateACHours(data[0].s);
    //     //this.calculateACHours2(data)
    //     for (let key in data) {

    //       let payload = {
    //         '1stAction': data[key]['1stAction'],
    //         '1stTime': this.calculateHours(data[key]['1stTime']),
    //         '2stAction': data[key]['2stAction'],
    //         '2stTime': this.calculateHours(data[key]['2stTime']),
    //         'VehicleName': data[key].VehicleName,
    //         'imei': data[key].imei,
    //         '_id': { 'imei': data[key]._id.imei }
    //       }
    //       this.reportData.push(payload)
    //     }
    //     // this.reportData = data;
    //     // console.log("1stAction: ", data[0]['1stAction'])

    //   })
    this.apiCAll.stopLoading();
    console.log("date---data ------->",new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
        this.apiCAll.getACReportForMobileAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
      .subscribe(data => {
        this.apiCAll.stopLoading();
        console.log("ac report dataddd: ", data)
      //  this.calculateACHours(data[0].s);
      this.isreportArray=false;
      let deviceIds = [];
      let temp = data[0].map((item)=>{
        console.log("thiss---->",this.reportArray)
       this.reportArray.push({deviceName:item.deviceName,workingHours:item.workingHours,stopHours:item.stopHours})
      })
      //this.reportArray = deviceIds.join(",");
      console.log(' this report array------- ',  this.reportArray);
        //this.reportArray.push({deviceName:data[0][0].deviceName,workingHours:data[0][0].workingHours,stopHours:data[0][0].stopHours});
        //this.calculateACHours2(data)
        console.log("data device name--->",data[0][0].deviceName);
        // for (let key in data) {

        //   let payload = {
        //     '1stAction': data[key]['1stAction'],
        //     '1stTime': this.calculateHours(data[key]['1stTime']),
        //     '2stAction': data[key]['2stAction'],
        //     '2stTime': this.calculateHours(data[key]['2stTime']),
        //     'VehicleName': data[key].VehicleName,
        //     'imei': data[key].imei,
        //     '_id': { 'imei': data[key]._id.imei }
        //   }
        //   this.reportData.push(payload)
        // }
        // this.reportData = data;
        // console.log("1stAction: ", data[0]['1stAction'])

      })

  }


//   workingHoursArray=[]
//  calculateACHours2(data){
//   console.log("hhhh",data);
//   var tempArry=[]
//   var stopHr=0;
//   var workinHr=0
//   var result = data.reduce(function (r, a) {
//         r[a.this.dev_id] = r[a.this.dev_id] || [];
//         r[a.this.dev_id].push(a);
//         return r;
//     }, Object.create(null));
//   console.log(result);

//   for(var i=0;i<data.length;i++){
//     for(var j=0;j<result[data[i]].length;j++){
//       console.log("1");

//       // if(j!==result[this.deviceArr[i]].length){
//          console.log("11",result[data[i]][j]);
//       if(result[data[i]][j].switch=="OFF"){
//          console.log("111",(result[data[i]][j].timestamp).replace('Z', '').replace('T', ''));
//          if(result[data[i]][j+1]){
//         if((new Date(result[data[i]][j].timestamp).getTime())>(new Date(result[data[i]][j+1].timestamp).getTime())){
//           workinHr=(new Date(result[data[i]][j].timestamp).getTime()-new Date(result[data[i]][j+1].timestamp).getTime())/1000;
//           stopHr=(new Date(this.datetimeEnd).getTime()-new Date(this.datetimeStart).getTime())/1000;

//         }
//       }
//       }
//     // }
//     }
//     tempArry.push({deviceName:result[data[i]][j-1].vehicleName,Device_ID:data[i],stopHours:this.secondsToHms(stopHr),workingHours:this.secondsToHms(workinHr)})
//     stopHr=0;
//     workinHr=0;
//   }
//   this.totalWorkingHours=tempArry
//   console.log("TEMP==>",tempArry);


// }

  workingHoursArray1=[]
  // calculateACHours(data: any) {
  //   //throw new Error('Method not implemented.');
  //   var total=0
  //   console.log(data);
  //   this.reportArray=[]
  //   for(let i=0;i<data.length;i++){
  //     if(i!==(data.length-1)){
  //       if(data[0].switch=="OFF"){
  //     if(data[i].switch=="OFF" && data[i+1].switch=="ON"){
  //       debugger
  //       var date=new Date((data[i].timestamp));
  //       var date1=new Date((data[i+1].timestamp))
  //       console.log("first", date, date1)
  //       this.workingHoursArray1.push({deviceName:data[i].vehicleName,timestamp:this.timeDifference(date,date1)})
  //     }
  //   }
  //   else{
  //     if(data[i].switch=="ON" && data[i+1].switch=="OFF"){
  //        var date=new Date((data[i].timestamp));
  //        var date1=new Date((data[i+1].timestamp))
  //        console.log("second", date, date1);
  //       this.workingHoursArray1.push({deviceName:data[i].vehicleName,timestamp:this.timeDifference(date,date1)})
  //     }
  //   }
  // }else{
  //   for(var j=0;j<this.workingHoursArray1.length;j++){
  //     total=total+this.workingHoursArray1[j].timestamp;
  //   }
  //  var time= Math.abs(this.timeDifference(this.datetimeStart,this.datetimeEnd));
  //         var stopHrs=this.secondsToHms((time-total))
  //   this.reportArray.push({deviceName:data[0].vehicleName,workingHours:this.secondsToHms(total),stopHours:stopHrs});
  //   }

  //   }
  //   console.log("JEDHJDH",this.workingHoursArray1,this.reportArray);
  // }

  timeDifference(date1,date2) {
    debugger
    var seconds = (new Date(date1).getTime() - new Date(date2).getTime()) / 1000;
    return seconds
  }

  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;

}

  // workingHoursArray=[]
  // async newMethod(data){
  //   console.log("hhhh",data);
  //   var tempArry=[]
  //   var stopHr=0;
  //   var workinHr=0
  //   var result = await data.reduce(function (r, a) {
  //         r[a.device.Device_ID] = r[a.device.Device_ID] || [];
  //         r[a.device.Device_ID].push(a);
  //         return r;
  //     }, Object.create(null));
  //   console.log(result);

  //   for(var i=0;i<data.length;i++){
  //     for(var j=0;j<result[this.deviceArr[i]].length;j++){
  //       console.log("1");

  //       // if(j!==result[this.deviceArr[i]].length){
  //          console.log("11",result[this.deviceArr[i]][j]);
  //       if(result[this.deviceArr[i]][j].switch=="OFF"){
  //          console.log("111",(result[this.deviceArr[i]][j].timestamp).replace('Z', '').replace('T', ''));
  //          if(result[this.deviceArr[i]][j+1]){
  //         if((new Date(result[this.deviceArr[i]][j].timestamp).getTime())>(new Date(result[this.deviceArr[i]][j+1].timestamp).getTime())){
  //           workinHr=(new Date(result[this.deviceArr[i]][j].timestamp).getTime()-new Date(result[this.deviceArr[i]][j+1].timestamp).getTime())/1000;
  //           stopHr=(new Date(this.todate).getTime()-new Date(this.Fromdate).getTime())/1000;

  //         }
  //       }
  //       }
  //     // }
  //     }
  //     tempArry.push({deviceName:result[this.deviceArr[i]][j-1].vehicleName,Device_ID:this.deviceArr[i],stopHours:this.secondsToHms(stopHr),workingHours:this.secondsToHms(workinHr)})
  //     stopHr=0;
  //     workinHr=0;
  //   }
  //   this.totalWorkingHours=tempArry
  //   console.log("TEMP==>",tempArry);


  // }

  showDetail(pdata) {
    console.log("pdata: ", pdata);
    let that = this;
    this.navCtrl.push(ACDetailPage, {
      'param': pdata,
      'fdate': that.datetimeStart,
      'tdate': that.datetimeEnd,
      'uid': that.islogin._id
    })
  }
}

@Component({
  templateUrl: './ac-detail.html',
  styles: [`
  .col {
    padding: 0px;
}`]
})
export class ACDetailPage implements OnInit {
  fdate: any;
  tdate: any;
  uid: any;
  paramData: any = {};
  ddata: any[] = [];
  vname: any;

  constructor(
    public apiCall: ApiServiceProvider,
    public navParam: NavParams
  ) {
    console.log("param parameters: ", this.navParam.get('param'))
    this.paramData = this.navParam.get('param');
    this.fdate = this.navParam.get('fdate');
    this.tdate = this.navParam.get('tdate');
    this.uid = this.navParam.get('uid');

  }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    debugger
    this.apiCall.startLoading().present();
    this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("detailed ac report data: ", data[0].s);
        this.ddata = data[0].s;
      })
  }
}
