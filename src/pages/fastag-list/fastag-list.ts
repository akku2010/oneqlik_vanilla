import { Component, OnInit } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { MsgUtilityPage } from '../msg-utility/msg-utility';
import { FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-fastag-list',
  templateUrl: 'fastag-list.html',
})
export class FastagListPage implements OnInit {
  islogin: any;
  fastagList: any[] = [];
  page: number = 1;
  DealerArraySearch: any = [];
  limit: number = 10000;
  CustomerArray: any;
  CustomerArraySearch: any = [];
  DCustomerArraySearch: any = [];
  CustomerData: any;
  CustomerDataa: any;
  msgForm: FormGroup;
  titleMsg: any;
  fullMsg: any;
  selectus: any = [];
  model: any
  selectDealer: any;
  dealer_firstname: any
  dealerId: any;
  cond: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    public navParam: NavParams
  ) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.msgForm = this.formBuilder.group({
      title: [this.islogin.fn],
      msg: [this.islogin.ln]
    });
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter FastagListPage');
  }

  ngOnInit() {
    this.getDealer();
    this.getcust();
    this.getAllDealers();
  }

  addFastag() {
    this.navCtrl.push('FastagPage', {

    });
  }
  getcust() {
    var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
    //this.apiCall.startLoading().present();
    this.apiCall.getCustomersCall(baseURLp)
      .subscribe(data => {
        //this.apiCall.stopLoading();
        this.CustomerData = data;
        this.CustomerArraySearch = [];
        this.CustomerArraySearch = this.CustomerData;

      },
        err => {
          this.apiCall.stopLoading();
          var a = JSON.parse(err._body);
          var b = a.message;
          let toast = this.toastCtrl.create({
            message: b,
            duration: 2000,
            position: "bottom"
          })
          toast.present();
          // toast.onDidDismiss(() => {
          //   this.navCtrl.setRoot('DashboardPage');
          // });
        });
  }

  getmodel(d) {
    debugger
    let pModal = this.modalCtrl.create(MsgUtilityPage, {
      param: d
    });
    pModal.onDidDismiss(data => {
      console.log("chff", data)
      this.selectus = data;
      this.cond = true;
    })
    pModal.present();

  }

  sendmsg() {
    debugger
    var data = {
      "messageTitle": this.titleMsg,
      "msgContent": this.fullMsg,
      "notificationType": "NOTIFICATION",
      "numberArr": this.selectus,
    }
    this.apiCall.startLoading().present();
    this.apiCall.announcementData(data)
      .subscribe((resp) => {
        this.apiCall.stopLoading();
        console.log(resp);

      })
  }

  getDealer() {
    this.page = 1;
    this.DealerArraySearch = [];
    this.apiCall.getDealers(this.islogin._id, this.page, this.limit)
      .subscribe(data => {
        this.DealerArraySearch = data;
      },
        err => {
          this.apiCall.stopLoading();
          console.log("getting error from server=> ", err);
          let toast = this.toastCtrl.create({
            message: 'No Dealer(s) found',
            duration: 2000,
            position: "bottom"
          })

          toast.present();

          toast.onDidDismiss(() => {
            this.navCtrl.setRoot("DashboardPage");
          })
        });
  }

  getList() {
    var url = this.apiCall.mainUrl + 'fastTag/getRequest?id=' + this.islogin.supAdmin + '&role=supAdmin';
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(url)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        console.log('respData: ', respData);
        if (respData.length > 0) {
          this.fastagList = respData;
        }
      },
        err => {
          this.apiCall.stopLoading();
        })
  }

  getAllDealers() {
    var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
    let toast = this.toastCtrl.create({
      message: 'Loading dealers..',
      position: 'bottom',
      duration: 1500
    });
    toast.present();
    this.apiCall.getAllDealerCall(baseURLp)
      .subscribe(data => {
        this.selectDealer = data;
        console.log("dealers list", this.selectDealer)
        // toast.dismiss();
      },
        error => {
          console.log(error)
        });
  }


  dealerOnChnage(dealer) {
    console.log(dealer);
    console.log(dealer.dealer_id);
    this.dealerId = dealer.dealer_id;
    this.getDealercust();
  }

  getDealercust() {
    var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.dealerId + '&pageNo=' + this.page + '&size=' + this.limit;
    //this.apiCall.startLoading().present();
    this.apiCall.getCustomersCall(baseURLp)
      .subscribe(data => {
        console.log("first click initial", this.DCustomerArraySearch)
        //this.apiCall.stopLoading();
        this.CustomerDataa = data;
        this.DCustomerArraySearch = [];
        this.DCustomerArraySearch = this.CustomerDataa;
        console.log("first click", this.DCustomerArraySearch)
        this.getmodel(this.DCustomerArraySearch);
      },
        err => {
          this.apiCall.stopLoading();
          var a = JSON.parse(err._body);
          var b = a.message;
          let toast = this.toastCtrl.create({
            message: b,
            duration: 2000,
            position: "bottom"
          })
          toast.present();
        });
  }

}
